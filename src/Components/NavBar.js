import React, { useEffect, useState } from 'react'
import './NavBar.css'

//* para crear el efecto de que es transparente al princpio y luego cambia de color
//* es necesario agregar un listener
const NavBar= ()=>{

    const [show, handleShow] = useState(false)

    useEffect(()=>{
        window.addEventListener("scroll",()=>{
            if(window.scrollY>200){ //? 550
                handleShow(true)//? que se muestre
            }else handleShow(false) //? o que no
        })
        return ()=>{ window.removeEventListener("scroll")}
    },[])

    return(
        <div className={`nav ${show && "nav__black"}`}>{/*/? ese && es como si fuera un 'entonces' */}
            <img className="nav__logo" src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.png" alt="Netflix Logo" />
            <img className="nav__avatar" src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/1bdc9a33850498.56ba69ac2ba5b.png" alt="Netflix Logo"/>
        </div>
    );
}

export default NavBar;