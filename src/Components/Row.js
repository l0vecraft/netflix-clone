import React, { useState, useEffect } from 'react'
import YouTube from 'react-youtube';
import movieTrailer from 'movie-trailer'
import axios from '../data/network/axios'
import './Row.css'

const baseURL = "http://image.tmdb.org/t/p/w185/";//otros tamaños 342, 500,780, original
const Row = ({ title, fetchUrl,isLargeRow }) => { //* se utiliza el destructuring directamente en las props
    const [movies, setMovies] = useState([]); //? recuerda, dentro del useState va el estado inicial de cualquier variable que yo asigne
    const [trailerUrl, setTrailerUrl] = useState("")

    //* use efect corre basado en una condicion/variable especifica

    useEffect(() => {//? este hook es como un didMount o didChange
        async function fetchData() {
            const req = await axios.get(fetchUrl)
            //* seteo mi array
            setMovies(req.data.results);
            return req
        }
        fetchData(); //* finalizada la funcion, la llamo
    }, [fetchUrl])//? la razon por la que se utiliza esa variable alli, es porque
    //? dicha variable viene desde fuera y debe notificarse
    const opts = {//* esta son las configuraciones basicas para el reproductor
        height: "390",
        width: "100%",
        playerVars:{
            autoplay:1
        }
    }
    const handleClick = (movie)=>{
        if(trailerUrl){
            setTrailerUrl('')
        }else{
            movieTrailer(movie?.name|| movie?.title || "")
            .then(url =>{
                //?para obtener una mejor explicacion de esto ve el video
                const urlParameters = new URLSearchParams(new URL(url).search)
                setTrailerUrl(urlParameters.get('v'))
            })
            .catch(error =>{
                console.error(error)
            })
        }
    }

    return (
        <div className="row"> {/** contenedor principal**/}
            <h2 className="title">{title}</h2>
            <div className="row__posters">
                {movies.map((movie, index) => {
                    return (
                        <div className="contImage">
                            <img src={`${baseURL}${isLargeRow?movie.poster_path: movie.backdrop_path}`} alt={movie.name} key={movie.id} onClick={()=> handleClick(movie)}/>
                        </div>
                    )
                })}
            </div>
           {trailerUrl && <YouTube videoId={trailerUrl} opts={opts} />}
        </div>
    );
}

export default Row;