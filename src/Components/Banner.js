import React  from 'react'
import axios from '../data/network/axios'
import requests from '../data/network/requests'
import { useState, useEffect } from 'react'
import './Banner.css'


const Banner =()=> {
    let baseURL = "http://image.tmdb.org/t/p/original"
    const [movie, setMovie] = useState([])
    useEffect(() => {
        async function fetchData(){
            const req = await axios.get(requests.fetchNetflixOriginals)
            setMovie(
                //esto solo se hara para sacar siempre 1 pelicula aleatoria
                req.data.results[
                    Math.floor(Math.random()* req.data.results.length-1)
                ]
            );
            return req;
        }
        fetchData()
    }, []);
    baseURL += movie?.backdrop_path; 
    
    console.log(baseURL)

    const truncate = (str, n)=>{
        /*
        *esta funcion lo unico que hace es el efecto de elipsis
        *en caso de que una descripcion sea muy larga dado un numero
        */
        return str?.length > n ? str.substr(0, n-1) +'...': str
    }
    
    return (
        <header style={{backgroundSize:"cover", backgroundImage:`url(${baseURL})`,backgroundPosition:"center center" }}>
            <div className="banner__contents">
                <h1 className="banner__title">
                    {movie?.name || movie?.title || movie?.original_name}
                </h1>
                <div className="banner__buttons">
                    <button className="banner__button">Play</button>
                    <button className="banner__button">My List</button>
                </div>
                <h1 className="banner__description">
                    {truncate(movie?.overview,200)}
                </h1>
            </div>
            <div className="banner__fadeBottom"></div>
        </header>
    )
}

export default Banner
