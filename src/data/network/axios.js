import axios  from "axios";

//* de esta forma se crearia siempre una instancia de una peticion en axios
//* lo unico que se le agregaria seria el endpoint

const instance = axios.create({
    baseURL:"https://api.themoviedb.org/3"
})
// instance.get(url)
export default instance;