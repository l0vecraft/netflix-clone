import React from 'react';
import Row from './Components/Row';
import Banner from './Components/Banner';
import requests from './data/network/requests';
import NavBar from './Components/NavBar';
import './App.css';

function App() {
  return (
   <div className="app">
     <NavBar />
     <Banner />
     <Row title="NETFLIX ORIGINALS" fetchUrl= {requests.fetchNetflixOriginals} isLargeRow={true}/>
     <Row title="Trending Movies" fetchUrl= {requests.fetchTrending} isLargeRow={true}/>
     <Row title="Top Rated" fetchUrl= {requests.fetchTopRated} />
     <Row title="Action Movies" fetchUrl= {requests.fetchActionMovies} />
     <Row title="Comedy Movies" fetchUrl= {requests.fetchComedyMovies} />
     <Row title="Horror Movies" fetchUrl= {requests.fetchHorrorMovies} />
     <Row title="Romance Movies" fetchUrl= {requests.fetchRomanceMovies} />
     <Row title="Documentaries" fetchUrl= {requests.fetchDocumentaries} />

   </div>
  );
}

export default App;
